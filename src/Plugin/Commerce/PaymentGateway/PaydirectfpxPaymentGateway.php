<?php

namespace Drupal\paydirectfpx\Plugin\Commerce\PaymentGateway;

use Drupal\commerce_order\Entity\OrderInterface;
use Drupal\commerce_payment\Exception\PaymentGatewayException;
use Drupal\commerce_payment\Plugin\Commerce\PaymentGateway\OffsitePaymentGatewayBase;
use Drupal\commerce_price\Price;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\HttpFoundation\Request;
use Drupal\commerce_order\Entity\Order;


/**
 * Provides the Off-site Redirect payment gateway.
 *
 * @CommercePaymentGateway(
 *   id = "paydirectfpx_redirect",
 *   label = "PayDirect FPX Payment Gateway",
 *   display_label = @Translation("paydirectfpx"),
 *    forms = {
 *     "offsite-payment" = "Drupal\paydirectfpx\PluginForm\OffsiteRedirect\PaymentOffsiteForm",
 *   },
 * )
 */
class PaydirectfpxPaymentGateway extends OffsitePaymentGatewayBase {


  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
        'merchant_id' => '',
        'private_key' => '',
        'api_url' => 'https://www.paydirectfpx.com/drupal_receiver.php',
      ] + parent::defaultConfiguration();
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildConfigurationForm($form, $form_state);

    $form['merchant_id'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Merchant id'),
      '#default_value' => $this->configuration['merchant_id'],
      '#required' => TRUE,
    ];
    $form['private_key'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Merchant API key'),
      '#default_value' => $this->configuration['private_key'],
      '#required' => TRUE,
    ];
    $form['api_url'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Endpoint URL'),
      '#default_value' => $this->configuration['api_url'],
      '#required' => TRUE,
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    parent::submitConfigurationForm($form, $form_state);
    if (!$form_state->getErrors()) {
      $values = $form_state->getValue($form['#parents']);
      $this->configuration['merchant_id'] = $values['merchant_id'];
      $this->configuration['private_key'] = $values['private_key'];
      $this->configuration['api_url'] = $values['api_url'];
    }
  }

  /**
   * {@inheritdoc}
   */
  // public function getDisplayLabel() {
  //   return $this->configuration['bank'] .' '. parent::getDisplayLabel();
  // }

  /**
   * {@inheritdoc}
   */
  public function onReturn(OrderInterface $order, Request $request) {
    if ($this->hash_pd_return($_POST)) {
      echo 'OK';
      if($_POST['msg'] == 'Success' || $_POST['msg'] == 'Transaction Approved') {

        $order = Order::load($_POST['invoice_no']);

        $payment_storage = $this->entityTypeManager->getStorage('commerce_payment');
        /** @var \Drupal\commerce_payment\Entity\Payment $payment */
            $payment = $payment_storage->create([
               'state' => 'completed',
               'amount' => $order->getTotalPrice(),
               'payment_gateway' => $this->entityId,
               'order_id' => $_POST['invoice_no'],
               'test' => $this->getMode(),
               'remote_id' => $_POST['VK_T_NO'],
               'remote_state' => $_POST['msg']
             ]);
             $payment->save();

      }
      else
      {
          $order = Order::load($_POST['invoice_no']);

          $payment_storage = $this->entityTypeManager->getStorage('commerce_payment');
          /** @var \Drupal\commerce_payment\Entity\Payment $payment */


          $payment = $payment_storage->create([
            'state' => 'authorization_voided',
            'amount' => $order->getTotalPrice(),
            'payment_gateway' => $this->entityId,
            'order_id' => $_POST['invoice_no'],
            'test' => $this->getMode(),
            'remote_state' => $_POST['msg']
          ]);

          $payment->save();

      }
    }

  }

  public function onNotify(Request $request) {

    if ($this->hash_pd_return($_POST)) {
      echo 'OK';
      if($_POST['msg'] == 'Success' || $_POST['msg'] == 'Transaction Approved') {

        $order = Order::load($_POST['invoice_no']);

        $payment_storage = $this->entityTypeManager->getStorage('commerce_payment');
        /** @var \Drupal\commerce_payment\Entity\Payment $payment */
            $payment = $payment_storage->create([
               'state' => 'completed',
               'amount' => $order->getTotalPrice(),
               'payment_gateway' => $this->entityId,
               'order_id' => $_POST['invoice_no'],
               'test' => $this->getMode(),
               'remote_id' => $_POST['VK_T_NO']." [".date('m/d/Y h:i:s a', time())."]",
               'remote_state' => $_POST['msg']
             ]);
             $payment->save();

      }
      else
      {
          $order = Order::load($_POST['invoice_no']);



          $payment_storage = $this->entityTypeManager->getStorage('commerce_payment');
          /** @var \Drupal\commerce_payment\Entity\Payment $payment */



          $payment = $payment_storage->create([
            'state' => 'Transaction Failed',
            'amount' => $order->getTotalPrice(),
            'payment_gateway' => $this->entityId,
            'order_id' => $_POST['invoice_no'],
            'test' => $this->getMode(),
            'remote_id' => $_POST['VK_T_NO']." [".date('m/d/Y h:i:s a', time())."]",
            'remote_state' => $_POST['msg']
          ]);

          $payment->save();

      }
    } else {
      throw new PaymentGatewayException('Invalid response');
    }
  }

  /**
   * Returns whether the bank signature matches the sent MAC
   *
   * @param array $data
   * @return bool
   */
  private function hash_pd_return(array $data) {

    if (!empty($data['hash']))
    {

      $hash_value = md5($this->configuration['private_key'].$data['fpx_fpxTxnId'].$data['invoice_no'].$data['txn_status'].$data['msg']);

      if($hash_value == $data['hash'])
      {
        return true;
      }
      else
      {
        return false;
      }
      // $signature_data = array();
      // if ($data['VK_SERVICE'] == '1111') {
      //   $signature_data = $this->padData([
      //     $data['VK_SERVICE'],
      //     $data['VK_VERSION'],
      //     $data['VK_SND_ID'],
      //     $data['VK_REC_ID'],
      //     $data['VK_STAMP'],
      //     $data['VK_T_NO'],
      //     $data['VK_AMOUNT'],
      //     $data['VK_CURR'],
      //     $data['VK_REC_ACC'],
      //     $data['VK_REC_NAME'],
      //     $data['VK_SND_ACC'],
      //     $data['VK_SND_NAME'],
      //     $data['VK_REF'],
      //     $data['VK_MSG'],
      //     $data['VK_T_DATETIME'],
      //   ]);
      // }
      //
      // if ($data['VK_SERVICE'] == '1911') {
      //   $signature_data = $this->padData([
      //     $data['VK_SERVICE'],
      //     $data['VK_VERSION'],
      //     $data['VK_SND_ID'],
      //     $data['VK_REC_ID'],
      //     $data['VK_STAMP'],
      //     $data['VK_REF'],
      //     $data['VK_MSG'],
      //   ]);
      // }
      //
      // $signature = base64_decode($data['VK_MAC']);
      // $public_key = @openssl_get_publickey($this->configuration['public_key']);
      // $out = @openssl_verify($signature_data, $signature, $public_key);
      // @openssl_free_key($public_key);
      //
      // return 1;
    }

    else
    {
      return false;
    }
  }

  /**
   * Returns the required padded output of the data array (3 digit variable stating the length of the element before every string)
   *
   * @param array $data
   * @return string
   */
  // private function padData(array $data) {
  //
  //   $output = '';
  //
  //   foreach ($data as $element) {
  //     $output .= str_pad(mb_strlen($element), 3, '0', STR_PAD_LEFT) . $element;
  //   }
  //
  //   return $output;
  // }

}
